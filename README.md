
# Free-fly-zero-case

OpenSCAD case for the free fly zero open vario.

## Description

Basic modular case for the free fly zero open vario project. Currently this is a quick hack based on the quick hack from the original model. This of course didn't increase OpenSCAD code quality. But as I only use it for the prototype so far I wanted to share it anyway.

## License

```
Free-fly-zero case

(C) Bernhard Guillon 2023
Licenced under (CC BY 4.0)

Based on "Modular raspberry pi case by jamesglanville"
Licenced under (CC BY 4.0)
https://github.com/JamesGlanville/Raspberry-Pi-case
https://www.thingiverse.com/thing:21733

https://creativecommons.org/licenses/by/4.0/
```
