// Free-fly-zero case

// (C) Bernhard Guillon 2023
// Licenced under (CC BY 4.0)

// Based on "Modular raspberry pi case by jamesglanville"
// Licenced under (CC BY 4.0)
// https://github.com/JamesGlanville/Raspberry-Pi-case
// https://www.thingiverse.com/thing:21733

// https://creativecommons.org/licenses/by/4.0/

// TODO: This file is a quick hack based on the quick hack from the original model.
//       This of course doesn't increase scad code quality. But as I only use it for the
//       prototype so far I wanted to share it anyway.

boardlength = 120;
height=30;
thickness=3;
groovedepth=1;
groovewidth=1.6;
basetobottom=5;
dcylinder=8;
dbore=3.6;
boardwidth=100;
cylinder_height=height/2+1;
top_to_e_paper=3;

module long_side()
{
difference()
{
	union()
	{
		translate([-thickness,0,0])cube([boardlength+2*thickness,height,thickness]);
		translate([-dcylinder/2,0,dcylinder/2])rotate([-90,0,0])cylinder(r=dcylinder/2,h=height);
		translate([boardlength-2+dcylinder/2,0,dcylinder/2])rotate([-90,0,0])cylinder(r=4,h=height);
	}

    // Add baseboard groove
	translate([0,basetobottom,-1])cube([boardlength,groovewidth+1,groovedepth+1]);

    // Remove left lower cylinder
	translate([-10,-1,0]) cube([10,cylinder_height,10]);
    // Remove right upper cylinder
	translate([boardlength-2,cylinder_height-1,0]) cube([10,cylinder_height,10]);

    // Drill a hole upper left cylinder
	translate([-4,15,4])rotate([-90,0,0])cylinder(r=1.8,h=40,center=true);
    // Drill a hole lower right cylinder

	translate([boardlength-2+4,15,4])rotate([-90,0,0])cylinder(r=1.8,h=40,center=true);

    // Add baseboard groove
	translate([0,height-(top_to_e_paper+groovewidth),-1])cube([boardlength,groovewidth+1,groovedepth+1]);
}
}

module long_side_raspberry()
{
difference()
{

	union()
	{
		translate([-thickness,0,0])cube([boardlength+2*thickness,height,thickness]);
		translate([-dcylinder/2,0,dcylinder/2])rotate([-90,0,0])cylinder(r=dcylinder/2,h=height);
		translate([boardlength-2+dcylinder/2,0,dcylinder/2])rotate([-90,0,0])cylinder(r=4,h=height);
	}

    // Add baseboard groove
	translate([0,basetobottom,-1])cube([boardlength,groovewidth+1,groovedepth+1]);

    // Remove left lower cylinder
	translate([-10,-1,0]) cube([10,cylinder_height,10]);
    // Remove right upper cylinder
	translate([boardlength-2,cylinder_height-1,0]) cube([10,cylinder_height,10]);

    // Drill a hole upper left cylinder
	translate([-4,15,4])rotate([-90,0,0])cylinder(r=1.8,h=40,center=true);
    // Drill a hole lower right cylinder
	translate([boardlength-2+4,15,4])rotate([-90,0,0])cylinder(r=1.8,h=40,center=true);

    // Add baseboard groove
	translate([0,height-(top_to_e_paper+groovewidth),-1])cube([boardlength,groovewidth+1,groovedepth+1]);

	// Remove Raspberry Ports dirty
	translate([boardlength-73,basetobottom+7,-1])cube([70,10,5]);
}
}


module short_side_raspberry()
{
difference()
{
	union() // sidewall with cylinders
	{
		translate([-thickness,0,0])cube([boardwidth+2*thickness,height,thickness]);
		translate([-dcylinder/2,0,dcylinder/2])rotate([-90,0,0])cylinder(r=dcylinder/2,h=height);
		translate([boardwidth-2+dcylinder/2,0,dcylinder/2])rotate([-90,0,0])cylinder(r=4,h=height);
	}

    // Add baseboard groove
	translate([0,basetobottom,-1])cube([boardwidth,groovewidth+1,groovedepth+1]);
    // Remove left lower cylinder
	translate([-10,-1,0]) cube([10,cylinder_height,10]);
    // Remove right upper cylinder
	translate([boardwidth-2,cylinder_height-1,0]) cube([10,cylinder_height,10]);

    // Drill a hole upper left cylinder
	translate([-4,15,4])rotate([-90,0,0])cylinder(r=1.8,h=40,center=true);
    // Drill a hole lower right cylinder
	translate([boardwidth-2+4,15,4])rotate([-90,0,0])cylinder(r=1.8,h=40,center=true);

    // Add e-ink groove
	translate([0,height-(top_to_e_paper+groovewidth),-1])cube([boardwidth,groovewidth+1,groovedepth+1]);

	// Remove Raspberry Ports dirty
	translate([1,basetobottom+7,-1])cube([34,10,5]);
}
}

module short_side()
{
difference()
{
	union() // sidewall with cylinders
	{
		translate([-thickness,0,0])cube([boardwidth+2*thickness,height,thickness]);
		translate([-dcylinder/2,0,dcylinder/2])rotate([-90,0,0])cylinder(r=dcylinder/2,h=height);
		translate([boardwidth-2+dcylinder/2,0,dcylinder/2])rotate([-90,0,0])cylinder(r=4,h=height);
	}

    // Add baseboard groove
	translate([0,basetobottom,-1])cube([boardwidth,groovewidth+1,groovedepth+1]);
    // Remove left lower cylinder
	translate([-10,-1,0]) cube([10,cylinder_height,10]);
    // Remove right upper cylinder
	translate([boardwidth-2,cylinder_height-1,0]) cube([10,cylinder_height,10]);

    // Drill a hole upper left cylinder
	translate([-4,15,4])rotate([-90,0,0])cylinder(r=1.8,h=40,center=true);
    // Drill a hole lower right cylinder
	translate([boardwidth-2+4,15,4])rotate([-90,0,0])cylinder(r=1.8,h=40,center=true);
}
}

module base()
{
	len = boardlength + 5;
	width = boardwidth + 4;

	difference()
	{
		union()
		{
			cube([len+5,width+5,3]);
			translate([1,1,0])cylinder(r=4,h=3);
			translate([1,width+4-1,0])cylinder(r=4,h=3);
			translate([len+4-1,1,0])cylinder(r=4,h=3);
			translate([len+4-1,width+4-1,0])cylinder(r=4,h=3);
		}
		translate([2,2,1.5])cube([len+1,width+1,1.5+1]);

		translate([1,1,-1])cylinder(r=1.8,h=10);
		translate([1,width+4-1,-1])cylinder(r=1.8,h=10);
		translate([len+4-1,1,-1])cylinder(r=1.8,h=10);
		translate([len+4-1,width+4-1,-1])cylinder(r=1.8,h=10);

		translate([1,1,1.5])cylinder(r=4.5,h=3);
		translate([1,width+4-1,1.5])cylinder(r=4.5,h=3);
		translate([len+4-1,1,1.5])cylinder(r=4.5,h=3);
		translate([len+4-1,width+4-1,1.5])cylinder(r=4.5,h=3);
	}
}

module top()
{
	len = boardlength + 5;
	width = boardwidth + 4;

	difference()
	{
		union()
		{
			cube([len+5,width+5,3]);
			translate([1,1,0])cylinder(r=4,h=3);
			translate([1,width+4-1,0])cylinder(r=4,h=3);
			translate([len+4-1,1,0])cylinder(r=4,h=3);
			translate([len+4-1,width+4-1,0])cylinder(r=4,h=3);
		}
		translate([2,2,1.5])cube([len+1,width+1,1.5+1]);

		translate([1,1,-1])cylinder(r=1.8,h=10);
		translate([1,width+4-1,-1])cylinder(r=1.8,h=10);
		translate([len+4-1,1,-1])cylinder(r=1.8,h=10);
		translate([len+4-1,width+4-1,-1])cylinder(r=1.8,h=10);

		translate([1,1,1.5])cylinder(r=4.5,h=3);
		translate([1,width+4-1,1.5])cylinder(r=4.5,h=3);
		translate([len+4-1,1,1.5])cylinder(r=4.5,h=3);
		translate([len+4-1,width+4-1,1.5])cylinder(r=4.5,h=3);

		// E-Paper
		translate([14, 9])cube([85,65,10]);

		// Vario left
		translate([14,width-9])cylinder(r=1,h=thickness);
		translate([14,width-7])cylinder(r=1,h=thickness);
		translate([16,width-9])cylinder(r=1,h=thickness);
		translate([16,width-7])cylinder(r=1,h=thickness);

		// Vario right
		translate([len-34,width-9])cylinder(r=1,h=thickness);
		translate([len-34,width-7])cylinder(r=1,h=thickness);
		translate([len-36,width-9])cylinder(r=1,h=thickness);
		translate([len-36,width-7])cylinder(r=1,h=thickness);

		// Speaker
		// FIXME: a loop or a generator would be nice ...
		translate([50,width-9])cylinder(r=1,h=thickness);
		translate([50,width-7])cylinder(r=1,h=thickness);

		translate([52,width-11])cylinder(r=1,h=thickness);
		translate([52,width-9])cylinder(r=1,h=thickness);
		translate([52,width-7])cylinder(r=1,h=thickness);
		translate([52,width-5])cylinder(r=1,h=thickness);

		translate([54,width-11])cylinder(r=1,h=thickness);
		translate([54,width-9])cylinder(r=1,h=thickness);
		translate([54,width-7])cylinder(r=1,h=thickness);
		translate([54,width-5])cylinder(r=1,h=thickness);

		translate([56,width-11])cylinder(r=1,h=thickness);
		translate([56,width-9])cylinder(r=1,h=thickness);
		translate([56,width-7])cylinder(r=1,h=thickness);
		translate([56,width-5])cylinder(r=1,h=thickness);

		translate([58,width-11])cylinder(r=1,h=thickness);
		translate([58,width-9])cylinder(r=1,h=thickness);
		translate([58,width-7])cylinder(r=1,h=thickness);
		translate([58,width-5])cylinder(r=1,h=thickness);

		translate([60,width-9])cylinder(r=1,h=thickness);
		translate([60,width-7])cylinder(r=1,h=thickness);

		// Keypad
		translate([len-24, width-70])cube([20,70,1]);
		translate([len-24, width-70+25])cube([4,22,thickness]);

	}
}

// Yes, currently you need to comment out the pices you need ^^

// long_side();
// long_side_raspberry();
// short_side_raspberry();
// base();
top();